package app.main;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.models.Tweet;
import app.models.Tweeter;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class TweetsAPI
{
  private String service_url = "http://localhost:9000";
  //public String service_url  = "http://10.0.2.2:9000";  //Android Emulator;
  //public String service_url  = "https://ianronan-mytweet-service.herokuapp.com/";
  
  private static MyTweetServiceProxy service;

  public TweetsAPI()
  {
    Gson gson = new GsonBuilder().create();

    Retrofit retrofit = new Retrofit.Builder().baseUrl(service_url)
                             .addConverterFactory(GsonConverterFactory
                             .create(gson))
                             .build();
    service = retrofit.create(MyTweetServiceProxy.class);
  }

  public static Tweet Tweet(String id, Tweet newTweet) throws Exception
  {
    Call<Tweet> call = (Call<Tweet>) service.createTweet(id, newTweet);
    Response<Tweet> returnedTweet = call.execute();
    return returnedTweet.body();
  }
  
  public static List<Tweeter> getAllTweeters() throws Exception
  {
    Call<List<Tweeter>> call = (Call<List<Tweeter>>) service.getAllTweeters();
    Response<List<Tweeter>> tweeters = call.execute();
    return tweeters.body();
  }

  public Tweeter getTweeter(String id) throws Exception
  {
    Call<Tweeter> call = (Call<Tweeter>) service.getTweeter(id);
    Response<Tweeter> tweeters = call.execute();
    return tweeters.body();
  }

  public static int deleteTweeter(String id) throws Exception
  {
    Call<Tweeter> call = service.deleteTweeter(id);
    Response<Tweeter> val = call.execute();
    return val.code();
  }

  public static int deleteAllTweeters() throws Exception
  {
    Call<String> call = service.deleteAllTweeters();
    Response<String> val = call.execute();
    return val.code();
  }

  public static Tweeter createTweeter(Tweeter newTweeter) throws Exception
  {
    Call<Tweeter> call = (Call<Tweeter>) service.createTweeter(newTweeter);
    Response<Tweeter> returnedTweeter = call.execute();
    return returnedTweeter.body();
  }

  public List<Tweet> getAllTweets() throws Exception
  {
    Call<List<Tweet>> call = (Call<List<Tweet>>) service.getAllTweets();
    Response<List<Tweet>> tweets = call.execute();
    return tweets.body();
  }

  public static List<Tweet> getTweets(String id) throws Exception
  {
    Call<List<Tweet>> call = (Call<List<Tweet>>) service.getTweets(id);
    Response<List<Tweet>> tweets = call.execute();
    return tweets.body();
  }

  public int deleteTweet(String id, String tweetId) throws Exception
  {
    Call<String> call = service.deleteTweet(id, tweetId);
    Response<String> val = call.execute();
    return val.code();
  }

  public int deleteAllTweets() throws Exception
  {
    Call<String> call = service.deleteAllTweets();
    Response<String> val = call.execute();
    return val.code();
  }

public static Tweet createTweet(String id, Tweet tweet) {
	// TODO Auto-generated method stub
	return null;
}
}