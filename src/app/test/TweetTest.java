package app.test;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import app.main.TweetServiceAPI;
import app.models.Tweet;
import app.models.Tweeter;

public class TweetTest
{
private Tweeter marge =  new Tweeter ("marge",  "simpson", "homer@simpson.com",  "secret");

private TweetServiceAPI tweetServiceAPI = new TweetServiceAPI();

@Before
public void setup() throws Exception
{ 
  marge = tweetServiceAPI.createTweeter(marge);
}

 @After
public void teardown() throws Exception
{
  tweetServiceAPI.deleteTweeter(marge.id);
}

@Test
public void testCreateTweet () throws Exception
{
  Tweet tweet = new Tweet ("Tweet1");
  Tweet returnedTweet = tweetServiceAPI.createTweet(marge.id, tweet);
  
  System.out.println(tweet);
  System.out.println(returnedTweet);
  
  assertEquals (tweet, returnedTweet);

  tweetServiceAPI.deleteTweet(tweet.id, returnedTweet.id);
}

@Test
public void testCreateTweets () throws Exception
{
  Tweet tweet1 = new Tweet ( "TweetA");
  Tweet tweet2 = new Tweet ( "TweetB");
  Tweet tweet3 = new Tweet ( "TweetC");

  Tweet returnedTweet1 = tweetServiceAPI.createTweet(marge.id, tweet1);
  Tweet returnedTweet2 = tweetServiceAPI.createTweet(marge.id, tweet2);
  Tweet returnedTweet3 = tweetServiceAPI.createTweet(marge.id, tweet3);

  assertEquals(tweet1, returnedTweet1);
  assertEquals(tweet2, returnedTweet2);
  assertEquals(tweet3, returnedTweet3);

  tweetServiceAPI.deleteTweet(tweet1.id, returnedTweet1.id);
  tweetServiceAPI.deleteTweet(tweet2.id, returnedTweet2.id);    
  tweetServiceAPI.deleteTweet(tweet3.id, returnedTweet3.id);
}

@Test
public void testListTweets () throws Exception
{
	  Tweet tweet1 = new Tweet ("TweetA");
	  Tweet tweet2 = new Tweet ("TweetB");
	  Tweet tweet3 = new Tweet ("TweetC");

	  tweetServiceAPI.createTweet(marge.id, tweet1);
	  tweetServiceAPI.createTweet(marge.id, tweet2);
	  tweetServiceAPI.createTweet(marge.id, tweet3);

  List<Tweet> tweets = tweetServiceAPI.getTweets(marge.id);
  assertEquals (3, tweets.size());

  assertTrue(tweets.contains(tweet1));
  assertTrue(tweets.contains(tweet2));
  assertTrue(tweets.contains(tweet3));

  tweetServiceAPI.deleteTweet(marge.id, tweets.get(0).id);
  tweetServiceAPI.deleteTweet(marge.id, tweets.get(1).id);    
  tweetServiceAPI.deleteTweet(marge.id, tweets.get(2).id);
}


}